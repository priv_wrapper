project(tests C)

if (TARGET cmocka::cmocka)
    set(CMOCKA_LIBRARY cmocka::cmocka)
endif()

include_directories(
  ${CMAKE_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_SOURCE_DIR}/src
  ${CMOCKA_INCLUDE_DIR}
)

set(PWRAP_TESTS
    test_setrlimit
    test_chroot)

if (HAVE_PRCTL)
list(APPEND PWRAP_TESTS test_prctl)
endif()

if (HAVE_PLEDGE)
list(APPEND PWRAP_TESTS test_pledge)
endif()

function(ADD_CMOCKA_TEST_ENVIRONMENT _TEST_NAME)
    if (CMAKE_BUILD_TYPE)
        string(TOLOWER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_LOWER)
        if (CMAKE_BUILD_TYPE_LOWER STREQUAL "addresssanitizer")
            find_library(ASAN_LIBRARY
                         NAMES asan)
            if (NOT ASAN_LIBRARY)
                foreach(version RANGE 10 1)
                    if (NOT ASAN_LIBRARY)
                        find_library(ASAN_LIBRARY libasan.so.${version})
                    endif()
                endforeach()
            endif()
        endif()
    endif()

    if (ASAN_LIBRARY)
        list(APPEND PRELOAD_LIBRARIES ${ASAN_LIBRARY})
    endif()
    list(APPEND PRELOAD_LIBRARIES ${PRIV_WRAPPER_LOCATION})

    if (OSX)
        set(TORTURE_ENVIRONMENT "DYLD_FORCE_FLAT_NAMESPACE=1;DYLD_INSERT_LIBRARIES=${PRIV_WRAPPER_LOCATION}")
    else ()
        string(REPLACE ";" ":" _TMP_ENV "${PRELOAD_LIBRARIES}")
        set(TORTURE_ENVIRONMENT "LD_PRELOAD=${_TMP_ENV}")
    endif()

    if (CMAKE_BUILD_TYPE)
        string(TOLOWER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_LOWER)
        if (CMAKE_BUILD_TYPE_LOWER STREQUAL "addresssanitizer" OR
            CMAKE_BUILD_TYPE_LOWER STREQUAL "threadsanitizer" OR
            CMAKE_BUILD_TYPE_LOWER STREQUAL "undefinedsanitizer")
            list(APPEND TORTURE_ENVIRONMENT "PRIV_WRAPPER_DISABLE_DEEPBIND=1")
        endif()
    endif()

    set_property(TEST
                    ${_TEST_NAME}
                PROPERTY
                    ENVIRONMENT "${TORTURE_ENVIRONMENT}")
endfunction()

foreach(_PWRAP_TEST ${PWRAP_TESTS})
    add_cmocka_test(${_PWRAP_TEST}
                    SOURCES ${_PWRAP_TEST}.c
                    COMPILE_OPTIONS -D_GNU_SOURCE
                    LINK_LIBRARIES ${CMOCKA_LIBRARY} PRIVATE defaults)
    add_cmocka_test_environment(${_PWRAP_TEST})
endforeach()
